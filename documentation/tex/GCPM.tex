\documentclass[doku.tex]{subfiles}

\begin{document}
	\chapter{Gray-Coded Bit Plane Matching}
		\label{chap:GCBPM}
		
		This chapter explains in detail how the chosen algorithm - Gray-Coded Bit-Plane Matching (GCBPM) - works and explains the two main ideas - Gray Code and Bit Planes - used by the algorithm.
	
		\section{Basic Idea}
			In this section the basic idea of the algorithm will be shown and explained. The procedure of matching and tracking different images consecutively can be separated into five tasks, which are shown in Figure \ref{fig:GCBPM_basic}. 
			
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.95\textwidth]{./img/GCBPM_basic}
				\caption{Block diagram of the GC-BPM image stabilization algorithm.}
				\label{fig:GCBPM_basic}
			\end{figure}
			
			\begin{enumerate}[label=\bfseries Step \arabic*:, leftmargin=1.7cm]
				\item Starting with preprocessing, the gray-coded bit-planes have to be produced out of the image. They are the input for the estimation of the 4 region local motion estimation. 
				
				\item This block estimates the motion using bit-planes, remarking that only few planes are needed. This is possible with using a highly efficient boolean binary operator for comparison of the images different bit-planes. 
				
				\item The next block uses the minimization of the error operator to produce a local motion vector for each of the four sub-images. Along with the former global motion vector this is subjected to a median operator. The outcome of the median is the current global motion vector estimation. 
				
				\item After integration, which removes high frequent distortions the filtered motion estimation is compensated by shifting the image with a number of pixels against the motion direction. 
			\end{enumerate}

			
		\section{Gray-Coded Bit Planes}
			\label{sec:GCBP}
			This section explains the ideas behind the Gray-Code and how to slice a given image into its Bit-Planes, as the algorithm uses both techniques in combination.
			
			\subsection{Gray Code}
				The Gray Code is another representation of binary numbers. The biggest difference to the standard representation is, that in Gray code, two numbers (neighbours in the decimal system) always only differ in one bit from each other. 
				
				\begin{table}[h!]
					\centering
					\begin{tabular}{ c | c || c }
						Decimal	& 	Binary 	& 	Gray Code \\
						\hline			
						0 		& 	000 	& 	000 \\
						1 		& 	001 	& 	001 \\
						2 		&	010		& 	011 \\
						3 		&	011 	& 	010 \\
						4 		&	100 	& 	110 \\
						5 		&	101 	& 	111 \\
						6 		&	110 	& 	101 \\
						7 		&	111 	& 	100 \\
						... 	&	... 	& 	... \\ 
					\end{tabular}
					\caption{Gray Code table for 8 Bits}
				\end{table}

				The Gray code can either be built by using tables like the one shown above or can be done very efficiently by using only bitwise operations. The following listings show the idea of how to use XOR's to generate Gray code from any binary number.
				\newline
				\lstinputlisting[caption=Gray code encoding implementation in C] {./src/gray.c}
				
				\lstinputlisting[caption=Gray code encoding implementation in Matlab, language=Matlab]{./src/grayEncode.m}
				
				A exemplary usage for Gray coded values are rotary encoders. If the encoder value (binary) is read parallel, often more than one bit will be changed from one step to the next. The problem is that those bits never change at the very same moment due to natural tolerances (error in the encoders disc, different propagation delays,...). If the encoder now reads the wrong value the result is completely off and can't be used further in the process. With Gray coded values this is not possible as explained beforehand.
				\newpage
				
			\subsection{Bit Planes}
			
				A bit plane of a digital discrete signal (such as image or sound) is a set of bits corresponding to a given bit position in each of the binary numbers representing the signal.	
				For example, for 8-bit data representation (e.g. gray scale image) there are 16 bit planes: the first bit plane contains the set of the most significant bit, and the 16th contains the least significant bit. The following Figure \ref{fig:bitplane} shows the decomposition of an gray scale image into its 8 bit planes, whereas in the listing the Matlab code can be seen.
				\newline
				\begin{figure}[h!]
					\centering
					\includegraphics[width=\textwidth]{./img/bitplanes}
					\caption{Image decomposition using bit planes.}
					\label{fig:bitplane}
				\end{figure}
				\newline
				\lstinputlisting[caption=Bit plane slicing implementation in Matlab, language=Matlab] {./src/getBitplanes.m}

				It is possible to see that the first bit plane gives the roughest but the most critical approximation of values of a medium, and the higher the number of the bit plane, the less is its contribution to the final stage. Thus, adding a bit plane gives a better approximation.
				
				As an exemplary usage of bit planes, image compression can be named. Replacement of more significant bits result in more distortion than replacement of less significant bits. In lossy media compression that uses bit-planes it gives more freedom to encode less significant bit-planes and it is more critical to preserve the more significant ones.	
	
			
			\subsection{Gray-Coded Bit Planes}
				As already mentioned the algorithm uses a combination of the two beforehand explained topics - Gray-Coded Bit Planes. This means, that after slicing the image into its 8 bit planes, a beforehand selected bit plane will be encoded in Gray code and further processed. Figure \ref{fig:bitplaneG} shows the outcome.
				\newline
				\begin{figure}[h!]
					\centering
					\includegraphics[width=\textwidth]{./img/bitplanesGray}
					\caption{Image decomposition using Gray coded bit planes.}
					\label{fig:bitplaneG}
				\end{figure}

				
				To be able to compare the results of the decomposition the same image as before was used. It is clearly visible, that the complexity of the transitions from one bit plane to the next is lower when the bit planes are Gray coded compared to the non-gray coded sequence in Figure \ref{fig:bitplane}. \cite[p. 50]{Majid1991}
				
				\newpage
				\lstinputlisting[caption=Gray coded bit plane slicing implementation in Matlab, language=Matlab] {./src/getGrayCodeBitPlane.m}
\end{document}