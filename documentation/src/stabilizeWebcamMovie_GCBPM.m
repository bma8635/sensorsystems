% filename:         stabilizeWebcamMovie_GCBPM.m
% original author:  Alan Brooks
% adapted by:       Fabian Ebenhoch
%                   Burkhard Martin

function [Ms,Va,Vg] = stabilizeWebcamMovie_GCBPM(M,Mg,Vg_prev,Va_prev)
    % init algorithm specific variables (uint8's for speed)
    N = 100;		% matching block size (assumed NxN square)
    p = (h/2-N)/2;	% max search window displacement
    nSteps = 3		% number of steps for log search
				% (3 is pixel, 2 is 2 pixel, etc...)
	...
	
    % pre-allocate variables for speed
    Cj = 1e9*ones(2*p+1);       % correlation measure
	...
    
    % break into 4 sub-images, (S1, S2, S3, S4)
    % SX(:,:,2,1) is S1 current image, SX(:,:,1,1) is past
    % could probably optimize as a reshape 
	S(:,:,1,1) = Mg( 1:h/2, 1:w/2, 1); % UL, S1
	...
	S(:,:,2,1) = Mg( 1:h/2, 1:w/2, 2); % UL, S1
	...

    for j = 1:4 % loop over sub-images
        if ~logSearchEnable
            % exhaustive search (slower, more precision)
            for m_pos = 1:2*p+1 % loop over possible displacements
                for n_pos = 1:2*p+1
                    % calculate correlation; could be very fast HW
                    bxor = bitxor(S(p+1:p+N,p+1:p+N,2,j), ...
					S(m_pos:m_pos+N-1,n_pos:n_pos+N-1,1,j) );
					% update correlation matrix
                    Cj(m_pos,n_pos) = sum(bxor(:)); 
                end
            end % displacement loops
            % find min Cj arguments: m_pos_min & n_pos_min
			...
        else
            % log search (faster)
            firstJmp = 4;
            prev_m_pos = 9; prev_n_pos = 9; % start in center
            for iter = 1:nSteps
                % iter'th step
                Cj = 1e9*ones(2*p+1); % reset correlation measure
                curJmp = firstJmp./2.^(iter-1);
                for m_pos = prev_m_pos-curJmp: ...
					curJmp: prev_m_pos + curJmp
                    for n_pos = prev_n_pos-curJmp: ...
					curJmp: prev_n_pos + curJmp
                        % calculate correlation
                        bxor = bitxor(S(p+1:p+N, p+1:p+N, 2, j), ...
					S(m_pos:m_pos+N-1, n_pos:n_pos+N-1, 1, j) );
					  % update correlation matrix
                        Cj(m_pos,n_pos) = sum(bxor(:));
                    end

                end
                % find min Cj arguments: m_pos_min & n_pos_min
                % and store for loop
				...
            end % iteration loop
        end % log search if
        % store motion vector for this sub-image
        V(j,:,1) = [m_pos_min n_pos_min]-p-1; 
    end % sub-image loop
	
	% calc current global motion vector
	Vg(1,:) = median([V(:,:,1);Vg_prev]);
	% apply damping to generate this frame's integrated motion vector
	Va(1,:) = D1 * Va_prev + Vg(1,:);

	% original translational correction
	% (not sub-pixel for now)
	r = round(Va(1,1)); % num rows moved
	c = round(Va(1,2)); % num columns moved
	Ms(max([1 1+r]):min([h h+r]),max([1 1+c]):min([w w+c]),1) = ...
	M(max([1 1-r]):min([h h-r]),max([1 1-c]):min([w w-c]),1);


return