I = M(:,:,1);
I_bit = M1bitGray;%getBitplanes(I);

figure;
subplot(2,5,[1]);
imshow(I, [])

subplot(2,5,2);
imshow(I_bit(:,:,8), [])

subplot(2,5,3);
imshow(I_bit(:,:,7), [])

subplot(2,5,4);
imshow(I_bit(:,:,6), [])

subplot(2,5,5);
imshow(I_bit(:,:,5), [])

subplot(2,5,7);
imshow(I_bit(:,:,4), [])

subplot(2,5,8);
imshow(I_bit(:,:,3), [])

subplot(2,5,9);
imshow(I_bit(:,:,2), [])

subplot(2,5,10);
imshow(I_bit(:,:,1), [])
% subplot(2,4,5);
% subplot(2,4,6);