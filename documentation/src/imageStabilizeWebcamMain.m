% filename: 		imageStabilizeWebcamMain.m
% original author:  Alan Brooks
% adapted version:  Fabian Ebenhoch
%                   Burkhard Martin

function [Mout] = imageStabilizeWebcamMain(cam)
	%% init algorithm specific variables (uint8's for speed)
	...
	while 1: 
		if i == 1
			% Gather 1st frame, Gray code
			pFrame = uint8(rgb2gray(snapshot(cam))); 
			% Convert to grayscale, calculate Gray code			
			pFrameg = uint8(getGrayCodeBitPlane(..));
			% build Matrix consisting from last 2 frames
			M(:, :, 1) = pFrame;
			Mg(:, :, 1) = pFrameg;
		else
			% Gather new frame, Gray code it
			...
			% build Matrix consisting from last 2 frames
			M(:, :, 2) = cFrame;
			Mg(:, :, 2) = cFrameg; 
			% build new motion vector
			Vg_prev = Vg;
			Va_prev = Va;
			% Call stabilization function 
			[Ms,Va,Vg] = stabilizeWebcamMovie_GCBPM(..);

			% Display stabilized stream
			...
		end
		i = 2
	end  
return