/*Gray code conversion
*
* To generate the Gray code the binary representation of the given 
* number gets linked with an XOR to the binary representation   
* shifted by one bit to the right.
*
* Example:
* 1011 XOR 
* 0101
* --------
* 1110
*/

int gray_encode(int n) {
    return n ^ (n >> 1);
}
