% orig author:      Alan Brooks
% adapted by:       Fabian Ebenhoch
%                   Burkhard Martin
% description:      Given gray scale matrix, M(width,height),
%                   this function returns the Gray coded bit plane.
% version history:  08-Mar-2003 created
%                   03-Jan-2016 adapted

function [Mg] = getGrayCodeBitPlane(M,bit)
    % Use either bit+1 or at max the 8th bit plane
    msb = min([bit+1 8]);
    % Orig. paper suggests 5th bit plane
    lsb = bit;

    w = length(M(1,:,1)); % width
    h = length(M(:,1,1)); % height
    % Preallocate for speed
    M1bit = zeros(h,w,8);
    M1bitGray = M1bit;
    
    for b = msb:-1:lsb
        % compute original bit-planes (1 is LSB, 8 is MSB)
        M1bit(:,:,b) = bitget(M(:,:),b);
        % compute gray coded bit-planes
        if b==8 % MSB
            M1bitGray(:,:,b) = M1bit(:,:,b);
        else % LSB's
            M1bitGray(:,:,b) = bitxor(M1bit(:,:,b),M1bit(:,:,b+1));
        end
    end
    
    % Single bit output value
    Mg = M1bitGray(:,:,bit);
return