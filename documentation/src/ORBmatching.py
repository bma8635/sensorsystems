import ...

img1 = cv2.imread('test1.jpg')            # queryImage
img2 = cv2.imread('test3.jpg')            # trainImage
# Initiate SIFT detector
orb = cv2.ORB_create()
# find the keypoints and descriptors with SIFT
kp1 = orb.detect(img1, None)
kp2 = orb.detect(img2, None)
kp1, des1 = orb.compute(img1, kp1)
kp2, des2 = orb.compute(img2, kp2)

# create BFMatcher object
bf = cv2.BFMatcher(cv2.NORM_HAMMING2, crossCheck=True)
# Match descriptors.
matches = bf.match(des1,des2)
# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)

# Draw first 10 matches.
...
cv2.imshow('result', newimg)
cv2.waitKey(0)