function [ng] = grayEncode(n)
	ng = bitxor(n, bitshift(n, -1))
return