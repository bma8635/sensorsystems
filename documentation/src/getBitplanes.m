% author:           Fabian Ebenhoch
%                   Burkhard Martin
% description:      Given gray scale matrix, I(width,height),
%                   this function returns the bit planes.
% version history:  03-Jan-2016 created

function [I_bit] = getBitplanes(I)
    w = length(I(1, :)); % width
    h = length(I(:, 1)); % height
    % Preallocate for speed
    I_bit = zeros(h,w,8);
    
    for b = 8: -1: 1
        % compute bit-planes (1 is LSB, 8 is MSB)
        I_bit(:, :, b) = bitget(I(:, :), b); 
    end
return