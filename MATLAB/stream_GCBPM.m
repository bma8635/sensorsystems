% filename:         stream_GCBPM.m
% original authors: Fabian Ebenhoch
%                   Burkhard Martin
% description:      showcase for GCBPM algorithm with streamed movie
% version history:
%                   29-Dec-2015 created

% clearing of workspace
clc;
clear;

%% stabilize video stream
fileName = 'shaky_car.avi';
Mout = imageStabilizeStreamMain(fileName, 0, 1);

%% assemble for playback of final movie
H2 = figure; 
set(H2,'name','generating final movie ...');
for i = 1:length([Ms(1,1,:)])
    imshow(Ms(:,:,i),[0 255]);
    movStab(i) = getframe(H2);
end

close(H2)

