% filename: imageStabilizeMain.m
% original author:  Alan Brooks
% adapted version:  Fabian Ebenhoch
%                   Burkhard Martin
% description:      dunno yet
% version history:
%                   06-Mar-2003 created
%                   08-Mar-2003 image I/O added
%                   09-Mar-2003 v0_1 archived
%                   10-Mar-2003 v0_2, v0_3 archived
%                   28-Dec-2015 renewed video import

function [] = imageStabilizeMain(fileName)
    % in no input file specified, default file will be chosen
    if ~exist('fileName','var')
        fileName = 'shaky_car.avi';
    end
    
    
    %% read file  
    readerobj = VideoReader(fileName, 'tag', 'myreader1');
    vidFrames = read(readerobj);
    nFrames = get(readerobj, 'NumberOfFrames');
    
    for k = 1 : nFrames
         mov(k).cdata = vidFrames(:,:,:,k);
    end
    
    %% setup figure and play original movie
    H1 = figure;
    set(H1, 'position', [150 150 readerobj.Width readerobj.Height])    
        
    movie(H1, mov, 1, readerobj.FrameRate);
    close(H1)
    
    %% convert to grayscale
    M = uint8(zeros(readerobj.Height,readerobj.Width,nFrames));
    for i = 1:nFrames
        M(:,:,i) = uint8(rgb2gray(mov(i).cdata));
    end

    %% do GC-BPM stabilization (gather statistics)
    tic
    
    [Ms,Va,Vg,V] = stabilizeMovie_GCBPM(M);
    
    t = toc; 
    
    fprintf('%.4f seconds per frame\n',t/(nFrames-1));
    %% assemble for playback of final movie
    H2 = figure; 
    set(H2,'name','generating final movie ...');
    
    for i = 1:length([Ms(1,1,:)])
        imshow(Ms(:,:,i),[0 255]);
        movStab(i) = getframe(H2);
    end
    
    close(H2)
    
    %% show final video
    
    H3 = figure; set(H3,'name','Final Stabilized Movie')
    imshow(Ms(:,:,1),[0 255]);
    curPos = get(H3,'position');
    set(H3, 'position', [150 150 curPos(3:4)]);
                    
    movie(H3,movStab,1,readerobj.FrameRate)
    %% save out final movie
    v = VideoWriter([fileName(1:end-4) '_out.avi']);
    open(v)
    writeVideo(v,movStab)
    close(v)

return