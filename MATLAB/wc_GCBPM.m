% clearing of workspace
% make sure camera is not used by any older process
clear all;
clc;
clear;

% open usb webcam interface
% usb webcam package must be installed
% only possible from 2015 further
cam = webcam

% setting cam properties for deterministic performance
cam.Resolution = '320x240';
cam.FocusMode = 'manual';
cam.Sharpness = 10;
cam.WhiteBalanceMode = 'manual';
cam.ExposureMode = 'manual';
cam.Brightness = 100;

% stabilize video stream
Mout = imageStabilizeWebcamMain(cam);



