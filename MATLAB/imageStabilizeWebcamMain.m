% filename: imageStabilizeMain.m
% original author:  Alan Brooks
% adapted version:  Fabian Ebenhoch
%                   Burkhard Martin
% description:      dunno yet
% version history:
%                   06-Mar-2003 created
%                   08-Mar-2003 image I/O added
%                   09-Mar-2003 v0_1 archived
%                   10-Mar-2003 v0_2, v0_3 archived
%                   29-Dec-2015 changed to support webcam stream

function [Mout] = imageStabilizeWebcamMain(cam)
    %% init algorithm specific variables (uint8's for speed)
    bit = 5;                % best grey code bit (det by paper)
    debug_disp = 0;         % no debugging output
    i = 1;
    
   for i = 1:200 
        if i == 1
            pFrame = uint8(rgb2gray(snapshot(cam))); %uint8(255*ones(240, 320, 3));
            M(:, :, 1) = pFrame;            
            pFrame = uint8(getGrayCodeBitPlane(pFrame, bit, 1, debug_disp));
            Mg(:, :, 1) = pFrame;
        else
            tic
            cFrame = uint8(rgb2gray(snapshot(cam)));%uint8(rgb2gray(mov(i).cdata)); %uint8(255*ones(240, 320, 3));
            M(:, :, 2) = cFrame;
            % convert to grayscale and calculate gray coded bitplane
            cFrameg = uint8(getGrayCodeBitPlane(cFrame, bit, 1, debug_disp));
            % build Matrix consisting from last 2 frames
            Mg(:, :, 2) = cFrameg; 
            % do GC-BPM stabilization for current frame (gather statistics)
            if i == 2
                Vg_prev = zeros(1,2);          %[0 0]; % global motion vector
                Va_prev = zeros(1,2);          % integrated motion vectors
            else
                Vg_prev = Vg(1,:);
                Va_prev = Va(1,:);
            end

            

            [Ms, Va, Vg, V, pFrame] = stabilizeWebcamMovie_GCBPM(M, Mg, Vg_prev, Va_prev);

            t = toc; 

            fprintf('%.4f seconds per frame\n',t);

            % assemble for playback of final movie
            subplot(1,2,1);
            imshow(M(:,:,1),[0 255]);
            title('Original');
            subplot(1,2,2);
            imshow(Ms(:,:,1),[0 255]);
            title('Stabilized');
            Mout(:,:,i) = Ms(:,:,1);
            M(:, :, 1) = cFrame;
            Mg(:, :, 1) = cFrameg;
        end
    end
        
return