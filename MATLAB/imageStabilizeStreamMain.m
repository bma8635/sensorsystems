% filename:         imageStabilizeStreamMain.m
% original author:  Alan Brooks
% adapted version:  Fabian Ebenhoch
%                   Burkhard Martin
% description:      dunno yet
% version history:
%                   06-Mar-2003 created
%                   08-Mar-2003 image I/O added
%                   09-Mar-2003 v0_1 archived
%                   10-Mar-2003 v0_2, v0_3 archived
%                   29-Dec-2015 changed to support webcam stream

function [Mout] = imageStabilizeStreamMain(fileName, showOutput, showComparison)
    %% init algorithm specific variables (uint8's for speed)
    bit = 5;                % best grey code bit (det by paper)
    debug_disp = 0;         % no debugging output
    if showOutput ==1 || showComparison == 1
        figure;
    end
    %% in no input file specified, default file will be chosen
    if ~exist('fileName','var')
        fileName = 'shaky_car.avi';
    end
    
    % read file  
    readerobj = VideoReader(fileName, 'tag', 'myreader1');
    vidFrames = read(readerobj);
    nFrames = get(readerobj, 'NumberOfFrames');
           
   for i = 1:nFrames 
        
        if i == 1
            pFrame = uint8(rgb2gray(vidFrames(:,:,:,i))); %uint8(255*ones(240, 320, 3));
            M(:, :, 1) = pFrame;            
            pFrame = uint8(getGrayCodeBitPlane(pFrame, bit, 1, debug_disp));
            Mg(:, :, 1) = pFrame;
        else
            tic
            cFrame = uint8(rgb2gray(vidFrames(:,:,:,i)));%uint8(rgb2gray(mov(i).cdata)); %uint8(255*ones(240, 320, 3));
            M(:, :, 2) = cFrame;
            % convert to grayscale and calculate gray coded bitplane
            %cFrame = uint8(rgb2gray(cFrame));
            cFrameg = uint8(getGrayCodeBitPlane(cFrame, bit, 1, debug_disp));
            % build Matrix consisting from last 2 frames
            Mg(:, :, 2) = cFrameg; 
            % do GC-BPM stabilization for current frame (gather statistics)
            if i == 2
                Vg_prev = zeros(1,2);          %[0 0]; % global motion vector
                Va_prev = zeros(1,2);          % integrated motion vectors
            else
                Vg_prev = Vg(1,:);
                Va_prev = Va(1,:);
            end

            

            [Ms, Va, Vg, V, pFrame] = stabilizeWebcamMovie_GCBPM(M, Mg, Vg_prev, Va_prev);

            t = toc; 

%             fprintf('%.4f seconds per frame for stabilizing\n',t);
            tic
            
            if showComparison == 1
                subplot(1,2,1);
                imshow(M(:,:,1),[0 255]);
                title('Original');
                subplot(1,2,2);
                imshow(Ms(:,:,1),[0 255]);
                title('Stabilized');
                drawnow 
                
                t_d = toc;
%                 fprintf('%.4f seconds per frame for showing\n',t_d);
            end
            
            if showOutput == 1
                imshow(Ms(:,:,1),[0 255]);
                title('Stabilized');
                drawnow
                
                t_d = toc;
            end
            
            t_d = toc;
            
            Mout(:,:,i) = Ms(:,:,1);
            M(:, :, 1) = cFrame;
            Mg(:, :, 1) = cFrameg;
            
            fprintf('%.4f secods per frame\n', t + t_d);
        end
        %i = i + 1;
    end
    
    
return