% filename: getGrayCodeBitPlane.m
% author: Alan Brooks
% description: Given gray scale double matrix, M(width,height,fr),
% this function returns the Gray coded bit plane, bit.
% version history:
% 08-Mar-2003 created
% optimized of debug_disp is off
function [Mg] = getGrayCodeBitPlane(M,bit,fr,debug_disp)
    if debug_disp
        % slow (all bits)
        msb = 8;
        lsb = 1;
    else
        % speedy
        msb = min([bit+1 8]);
        lsb = bit;
    end
    
    w = length(M(1,:,1)); % width
    h = length(M(:,1,1)); % height
    % useful: bitget, dec2bin, num2bin, bitxor
    M1bit = zeros(h,w,8);
    M1bitGray = M1bit;
    
    for b = msb:-1:lsb
        % compute original bit-planes (1 is LSB, 8 is MSB)
        M1bit(:,:,b) = bitget(M(:,:,fr),b); % fr'th frame
        % compute gray coded bit-planes
        if b==8 % MSB
            M1bitGray(:,:,b) = M1bit(:,:,b);
        else % LSB's
            M1bitGray(:,:,b) = bitxor(M1bit(:,:,b),M1bit(:,:,b+1));
        end
    end
    
% Single bit output value
Mg = M1bitGray(:,:,bit);

% Debugging displays
    if debug_disp
        for b = 8:-1:1
        figure,imshow(M(:,:,fr),[0 255]) % orig
        figure,set(gcf,'name',sprintf('Bit-Plane for bit # %d',b))
        imshow(M1bit(:,:,b)) % normal
        figure,set(gcf,'name',sprintf('Gray Bit-Plane for bit # %d',b))
        imshow(M1bitGray(:,:,b)) % gray
        end
    end
return